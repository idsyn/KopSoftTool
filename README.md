# KopSoftTool

#### 介绍
KopSoftTool:二维码标签打印程序,C#串口通信SerialPort,SCADA监控与数据采集 西门子PLC OPC,等

作者网站 http://www.kopsoft.cn/

#### 软件架构

Microsoft .NET Framework 4.5

        public void Print(int Number)
        {
            pd.DefaultPageSettings.PaperSize = new PaperSize("", 999, 999); //设置纸张大小
            StandardPrintController controler = new StandardPrintController();

            if (dataGridView1.CurrentCell != null)
            {
                for (int j = 0; j < dataGridView1.SelectedRows.Count; j++) //遍历所有选中的行
                {
                    rowIndex = dataGridView1.SelectedRows[j].Index;
                    try
                    {
                        pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                        pd.PrintController = controler;
                        for (int i = 0; i < Number; i++)
                        {
                            pd.Print();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return;
                    }
                    finally
                    {
                        pd.Dispose();
                    }
                }
            }
        }



#### 使用说明

二维码标签打印程序
C#打印 1.建立PrintDocument对象2.设置PrintPage打印事件3.调用Print方法进行打印
ZXing.Net

C#串口通信SerialPort
串口按电气标准及协议来划分，包括RS-232-C、RS-422、RS485
RXD 接收数据 Receive Data
TXD 发送数据 Transmit Data
SGND 信号接地 Signal Ground

SCADA监控与数据采集
西门子PLC OPC
C#通过OPC Server自定义接口实现客户端数据读写
在客户端开发时，要使用OpcServer对象来实现客户端与Opc服务器之间的连接。一个OpcServer对象下有多个OpcGroup，一个OpcGroup下有多个OpcItem，
在自定义接口下的Client开发，是以Group为单位的操作，数据读写都是通过OpcGroup进行的。
引用OpcRcw.Comn.dll OpcRcw.Da.dll

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)